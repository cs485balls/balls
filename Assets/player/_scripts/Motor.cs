﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Sphere controls - N3K EN

public class Motor : MonoBehaviour
{
    public float moveSpeed = 5.0f; //Roll speed value
    public float drag = 0.5f; //Friction value
    public float terminalRotationSpeed = 25.0f; //Fastest a player can go.
    public float gravity = 9.8f;
    public Transform feet;
    public LayerMask ground;
    private float disToGround;
    private bool isGrounded;
    private CharacterController charControl;
	private bool init = true;

	private bool playerStatus; //Player is dead or alive.
    //Ball type. Used for communication with objects.
    //1 - Bowling Ball
    //2 - Basket Ball
    //3 - Soccer Ball
    //4 - Tennis Ball
    private int ballType;

	public Text screenText; //Any text that needs to be displayed on the screen for the player to see.

	private MeshFilter mesh; //Which mesh is being selected.

	public Mesh[] balls;

    public float jumpHeight;

    private Rigidbody PlayerRB; //Rigidbody of the player.

    private Transform camTransform; //Camera transform reference

    private Transform PlayerTrans; //Transform of the player

    private Collider PlayerColl; //Physics Material of the player

	private float time = 10.0f; //pause time

	private SphereCollider BallCollision; //Actual sphere collider of object.

    private Renderer rend;

	public MenuScript inGameMenus;

	public static AudioClip jump, changeShape, victory;
	static AudioSource audioSrc;

/*	public AudioSource jumpSound;
	public AudioSource changeSound; 
	//public AudioClip rollsound; */

    //Player Materials
    public Material[] materials;

    private Vector3 fallingVelocity;

	private bool playerHasWon;



	Scene CurrentScene;

	/* Wait functions */

    //Some utility functions. |Used for object communication.
    public bool getGrounded()
    {
        return isGrounded;
    }

    public int getBallType()
    {
        return ballType;
    }

	//Motor menu scripts

	public void playerWins()
	{
		playerHasWon = true;
	}
	//Player death scripts

	public void playerHasDied()
	{
		playerStatus = false;
	}

	public bool playerIsAlive()
	{
		return playerStatus;
	}
	//Plays sounds directly related to the ball
	public static void PlaySound(string clip)
	{
		switch (clip)
		{
		case "jump":
			audioSrc.PlayOneShot(jump);
			break;
		case "change":
			audioSrc.PlayOneShot(changeShape);
			break;
		case "win":
			audioSrc.PlayOneShot(victory);
			break;
		}
	}

    //Allows the change of the balls
	void changeProperties(int newMaterial, Vector3 newScale, float collisionRadius, float newMass, float newBounciness, float newSpeed, float newJumpHeight, int type)
	{
		if(!init) //if the level is not being initalized
			PlaySound("change");
		//Mesh
		mesh.sharedMesh = balls[newMaterial];
		//Radius size
		BallCollision.radius = collisionRadius;
		//Material
		rend.sharedMaterial = materials[newMaterial];
		//Size
		PlayerTrans.localScale = newScale;
		//Mass
		PlayerRB.mass = newMass;
		//Bounciness
		PlayerColl.material.bounciness = newBounciness;
		//Speed
		moveSpeed = newSpeed;
		//Jump height
		jumpHeight = newJumpHeight;
		//ball type
		ballType = type;
	}

	public void ResetText()
	{
		screenText.text = "";
	}
	public void displayText(string message)
	{
		screenText.text = message; 
		Invoke("ResetText", 5);
	}

    private void Start()
    {
        //Initalize all components
		Time.timeScale = 1.0f;
		playerStatus = true;
		playerHasWon = false;

		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;

		BallCollision = GetComponent<SphereCollider>();
		mesh = GetComponent<MeshFilter>();
        rend = GetComponent<Renderer>();
        PlayerRB = GetComponent<Rigidbody>();
        PlayerTrans = GetComponent<Transform>();
        PlayerColl = GetComponent<Collider>();
        charControl = GetComponent<CharacterController>();
		CurrentScene = SceneManager.GetActiveScene();

		jump = Resources.Load<AudioClip>("jump");
		changeShape = Resources.Load<AudioClip>("transform");
		victory = Resources.Load<AudioClip>("pop");
		audioSrc = GetComponent<AudioSource>();

		Debug.Log(CurrentScene.name);
        //set inital values
        PlayerRB.maxAngularVelocity = terminalRotationSpeed; 
        PlayerRB.drag = drag;
        fallingVelocity = Vector3.up;
        changeProperties(
				0, //Which material to change to.
                new Vector3(12.0f, 12.0f, 12.0f), //Size
			    0.04921262f, //Collision sphere size
                2.0f, //Mass
                0.1f, //Bounciness
                15.0f, //Speed
                2.0f, //Jump Height
                1 //Ball Type
		);
		init = false;
		inGameMenus.setCurrentSceneName(CurrentScene.name, CurrentScene.buildIndex);

    }

    private void Update()
    {
        //Change the character will go here.
		if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{ //Bowling Ball
			//change sound effect
			//soundManager.PlaySound("change");

			//Debug.Log("1");
			changeProperties (
				0, //Which material to change to.
				new Vector3 (12.0f, 12.0f, 12.0f), //Size
				0.04921262f, //Collision sphere size
				2.0f, //Mass
				0.001f, //Bounciness
				15.0f, //Speed
				2.0f, //Jump Height
				1 //Ball Type
			);
                       
		} 
		else if (Input.GetKeyDown (KeyCode.Alpha2))
		{ //Basket Ball
			//change sound effect
			//soundManager.PlaySound("change");

			//Debug.Log("2");
			changeProperties (
				1, //Which material to change to.
				new Vector3 (3.0f, 3.0f, 3.0f), //Size
				0.1208323f, //Collision sphere size
				1.0f, //Mass
				0.4f, //Bounciness
				5.0f, //Speed
				12.0f, //Jump Height
				2 //Ball Type
			);
		} 
		else if (Input.GetKeyDown (KeyCode.Alpha3)) 
		{ //Soccer Ball
			//change sound effect
			//soundManager.PlaySound("change");

			//Debug.Log("3");
			changeProperties (
				2, //Which material to change to.
				new Vector3 (100f, 100f, 100f), //Size
				0.003449997f, //Collision sphere size
				1.5f, //Mass
				0.01f, //Bounciness
				20.0f, //Speed
				2.5f, //Jump Height
				3 //Ball Type
			);
		} 
		else if (Input.GetKeyDown (KeyCode.Alpha4)) 
		{ //Tennis Ball
			//change sound effect
			//soundManager.PlaySound("change");

			//Debug.Log("4");
			changeProperties (
				3, //Which material to change to.
				new Vector3 (8f, 8f, 8f), //Size
				0.03436995f, //Collision sphere size
				0.9f, //Mass
				0.01f, //Bounciness
				6.0f, //Speed
				5.0f, //Jump Height
				4 //Ball Type
			);
		} 
/*		else if (Input.GetKeyDown (KeyCode.R)) 
		{ //Debug: Reset the scene
			string name = CurrentScene.name;
			SceneManager.LoadScene (name);
		} */
		else if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			inGameMenus.PauseMenu();

		}

		if (playerStatus == false) 
		{
			inGameMenus.DeathMenu();
		}

		if (playerHasWon == true) 
		{
			inGameMenus.WinMenu();
		}

    }

    private void FixedUpdate()
    {
        Vector3 dir = Vector3.zero; //Direction Vector
        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if(dir.magnitude > 1)
        {
            dir.Normalize(); //Don't go faster on diagonal axis. Refer to CS 485 Lectures.
        }
        //Rotate direction vector w/ camera
        camTransform = Camera.main.transform;

        Vector3 rotatedDirection = camTransform.TransformDirection(dir); //Transforms the direction w.r.t the camera
        rotatedDirection = new Vector3(rotatedDirection.x, 0, rotatedDirection.z);
        rotatedDirection = rotatedDirection.normalized * dir.magnitude;

//        bool isGrounded = Physics.CheckSphere(feet.position, 0.5f, ground, QueryTriggerInteraction.Ignore);
//        Debug.Log(isGrounded);
        PlayerRB.AddForce(rotatedDirection * moveSpeed ); //Move the object
		if (Input.GetButtonDown ("Jump") && isGrounded) { //hop
			PlayerRB.velocity += jumpHeight * Vector3.up;
			Debug.Log ("hop");

            //jump sound
            PlaySound("jump");
        }
        else if(Input.GetButton("Jump") && isGrounded) //repeat the jump
        {
            PlayerRB.velocity += jumpHeight * Vector3.up;
			Debug.Log ("Hippty hop");

            //jump sound
			PlaySound("jump");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Entered");
        if(collision.gameObject.CompareTag("ground"))
        {
            isGrounded = true;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("Exited");
        if (collision.gameObject.CompareTag("ground"))
        {
            isGrounded = false;
        }
    }

}


