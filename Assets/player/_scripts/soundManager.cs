﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour {

    public static AudioClip jump, changeShape, victory;
    static AudioSource audioSrc;

    // Use this for initialization
    void Start () {
        jump = Resources.Load<AudioClip>("Boing-sound-effect");
        changeShape = Resources.Load<AudioClip>("Cartoon-pop");
        victory = Resources.Load<AudioClip>("Quiz-correct-win-victory-sound-effect");
        audioSrc = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "jump":
                audioSrc.PlayOneShot(jump);
                break;
            case "change":
                audioSrc.PlayOneShot(changeShape);
                break;
            case "win":
                audioSrc.PlayOneShot(victory);
                break;
        }
    }
}
