﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{

	public GameObject pauseMenu;

	public GameObject deathMenu;

	public GameObject victoryMenu;

	private string name;

	private int sceneIndex;

	public Motor player;

	public void setCurrentSceneName(string passedName, int passedNumber)
	{
		name = passedName;
		sceneIndex = passedNumber;

	}


	public Button[] pauMenu;
	public Button[] deaMenu;
	public Button[] vicMenu;


	private void Start()
	{
		pauMenu [0].onClick.AddListener(OnResume);
		pauMenu [1].onClick.AddListener(OnQuit);
		deaMenu [0].onClick.AddListener(OnReset);
		deaMenu [1].onClick.AddListener(OnQuit);
		vicMenu [0].onClick.AddListener(OnNextLevel);
		vicMenu [1].onClick.AddListener(OnQuit);
	}

	private void OnNextLevel()
	{
		Time.timeScale = 1.0f;
		if (sceneIndex < 6)
			SceneManager.LoadScene (sceneIndex + 1);
		else
			SceneManager.LoadScene (0);
	}

	private void OnQuit()
	{
		Time.timeScale = 1.0f;
		SceneManager.LoadScene("startMenu");
	}

	public void WinMenu()
	{
		Time.timeScale = 0.0f;
		victoryMenu.SetActive(true);
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}

	private void OnReset()
	{
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		SceneManager.LoadScene (name);
	}

	public void DeathMenu()
	{
		deathMenu.SetActive(true);
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}

	private void OnResume()
	{
		Time.timeScale = 1.0f;
		pauseMenu.SetActive(false);
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;

	}

	public void PauseMenu()
	{
		Time.timeScale = 0.0f;
		pauseMenu.SetActive(true);
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}
}
