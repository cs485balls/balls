﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class death : MonoBehaviour 
{

	public Motor player;

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			player.playerHasDied(); //The player has contacted this trigger, and is therefore dead.
		}
	}
}
