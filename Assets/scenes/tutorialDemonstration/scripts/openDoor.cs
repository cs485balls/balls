﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openDoor : MonoBehaviour {

    public Motor motor; //Refer to the player motor script.

    public GameObject lever; //Lever Object
    private Animation leverAnimation; //Lever Animation

    public GameObject door; //Door Object
    private Animation doorAnimation; //Door animation

    private void Start()
    {
        leverAnimation = lever.GetComponent<Animation>();
        doorAnimation = door.GetComponent<Animation>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("A collision has indeed occured!");
        if(other.gameObject.tag == "Player") //If the player is contacting the button
        {
            Debug.Log("And the player is contacting the lever!");
           // Debug.Log(motor.getBallType());
            if(motor.getBallType() == 1) //If the player is a bowling ball
            {
                Debug.Log("And the player is a bowling ball!");
                //Play the animations
                leverAnimation.Play();
                doorAnimation.Play();
            }
        }
        else
        {
           Debug.Log("But it is NOT a player collision!");
        }
    }
}
