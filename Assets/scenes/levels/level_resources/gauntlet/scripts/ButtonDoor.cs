﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonDoor : MonoBehaviour 
{
	public GameObject[] DissapearingObjects;

	public GameObject[] AppearingObjects;

	private GameObject Button; //Script is applied to the button object.

	// Use this for initialization
	void Start () 
	{
		Button = GetComponent<GameObject>();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player") 
		{
			for(int i = 0; i < DissapearingObjects.Length; i++)
			{
				DissapearingObjects [i].SetActive (false);
			}
			for(int i = 0; i < AppearingObjects.Length; i++)
			{
				AppearingObjects [i].SetActive (true);
			}

		}
	}
}
