﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour 
{
	//Menus
	public Button[] firstMenu;
	public Button[] secondMenu;
	public Button[] PauseMenu;
	public Button[] LoseMenu;

	//Menu Game Objects
	public GameObject[] Menus;

	public Transform[] transforms;


	private Transform cameraTransform;
	private Transform cameraDesiredLookAt;

//    public static AudioClip buttonPush;
 //   static AudioSource audioSrc;

	private void LookAT(Transform t)
	{
		cameraDesiredLookAt = t;
	}

    private void Start()
    {

/*        buttonPush = Resources.Load<AudioClip>("pop");
 * 
 * 
  
        audioSrc = GetComponent<AudioSource>(); */
		Time.timeScale = 1.0f;
		cameraTransform = Camera.main.transform;
		firstMenu [0].onClick.AddListener (OnPlay);
		firstMenu [1].onClick.AddListener (OnQuit);
		secondMenu [0].onClick.AddListener (Tutorial);
		secondMenu [1].onClick.AddListener (LevelOne);
		secondMenu [2].onClick.AddListener (LevelTwo);
		secondMenu [3].onClick.AddListener (LevelThree);
		secondMenu [4].onClick.AddListener (LevelFour);
		secondMenu [5].onClick.AddListener (LevelFive);
		secondMenu [6].onClick.AddListener (OnBack);
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;

    }

	private void SETTONULL()
	{
		cameraDesiredLookAt = null;
	}

	private void Update()
	{
/*		if (cameraDesiredLookAt != null) 
		{
			Debug.Log ("DesiredLookAt != NULL!");
			cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, cameraDesiredLookAt.rotation, 1.5f * Time.deltaTime);
		} */
	
	}

	void ShowMenuOne()
	{

	}

	void OnPlay()
	{
		//hide first menu
		Menus[0].SetActive(false);
		//Delete first menu item
		//transform the camera 180 degrees
		cameraTransform.LookAt(transforms[1]);
		//cameraDesiredLookAt = transforms[1];
		//wait 5 seconds
		//load second menu when camera stops
		//Invoke("ShowMenuOne", 1.0f);
		Menus[1].SetActive(true);
	}

	void OnQuit()
	{
		//quit the game
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}

	void LevelOne()
	{
		//load first level
		SceneManager.LoadScene(2);
	}

	void LevelTwo()
	{
		//load second level
		SceneManager.LoadScene(3);
	}

	void LevelThree()
	{
		//load third level
		SceneManager.LoadScene(4);
	}

	void LevelFour()
	{
		//load fourth level
		SceneManager.LoadScene(5);
	}

	void LevelFive()
	{
		//load fifth level
		SceneManager.LoadScene(6);
	}

	void Tutorial()
	{
		//load tutoriallevel
		SceneManager.LoadScene("tutorial4");
	}

	void ShowMenuTwo()
	{

	}

	void OnBack()
	{
		//Hide second menu
		Menus[1].SetActive(false);
		//Transform the camera to 0 degrees
		cameraTransform.LookAt(transforms[0]);
		//cameraDesiredLookAt = transforms[0];
		//Wait 5 seconds
		//Invoke("ShowMenuTwo", 1.0f);
		//Load first menu when camera stops
		Menus[0].SetActive(true);
	}

/*    public void LoadScene(string name)
    {
        audioSrc.PlayOneShot(buttonPush);
        Application.LoadLevel(name);
    } 

    public void QuitGame()
    {
        Application.Quit();
    }
    */
} 
