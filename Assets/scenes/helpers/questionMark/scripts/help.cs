﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class help : MonoBehaviour 
{

	private Text helpText;

	public Motor player;

	public int selectMessage;

	public string selectWhichMessageToSend()
	{
		switch (selectMessage) 
		{
		case 0:
			return "Press 1 to change to the bowling ball.\n It can be used to roll between gutters!";
			break;
		case 1:
			return "Press 2 to change to the basket ball.\n It can be used to Jump!";
			break;
		case 2:
			return "Press 4 to change to the tennis ball.\nIt can be used to get through tight spaces!";
			break;
		case 3:
			return "Press 3 to change to the soccer ball.\nIt can be used to clear large gaps!";
			break;
		case 4:
			return "Return to the center.\nMore paths have opened up!";
			break;
		case 5:
			return "Looks like a smaller ball will serve well here...";
			break;
		case 6:
			return "Try using a bouncier ball to clear these jumps...";
			break;
		case 7:
			return "You'll need some serious speed to clear that gap!";
			break;
		case 8:
			return "You CANNOT jump on gray platforms!";
			break;
		case 9:
			return "It is up to you to determine which ball to use!";
			break;
		default:
			return "";
		}
	
	}


	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
			string message = selectWhichMessageToSend();
			player.displayText(message);
			Destroy(gameObject);
		}
	
	}
}
